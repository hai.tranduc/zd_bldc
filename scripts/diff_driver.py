#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
import rospy
import rospkg
from std_msgs.msg import Int64, Int16, Int8, String, Empty
from geometry_msgs.msg import Twist

common_folder = os.path.join(rospkg.RosPack().get_path('agv_common_library'), 'scripts')
if not os.path.isdir(common_folder):
    common_folder = os.path.join(rospkg.RosPack().get_path('agv_common_library'), 'release')
sys.path.insert(0, common_folder)
from common_function import *

"""

 #    #   ##   # #    #     ####  #        ##    ####   ####
 ##  ##  #  #  # ##   #    #    # #       #  #  #      #
 # ## # #    # # # #  #    #      #      #    #  ####   ####
 #    # ###### # #  # #    #      #      ######      #      #
 #    # #    # # #   ##    #    # #      #    # #    # #    #
 #    # #    # # #    #     ####  ###### #    #  ####   ####

"""
class DiffDriver():
    def __init__(self):
        rospy.init_node('diff_driver')
        rospy.loginfo('Init node diff_driver')

        self.init_varialble()
        self.init_ros()
        rate = rospy.Rate(10.0)
        while not rospy.is_shutdown():
            self.poll()
            rate.sleep()

    def init_varialble(self):
        self.wheel_diameter = rospy.get_param('~wheel_diameter', 0.15)
        self.wheel_separation = rospy.get_param('~wheel_separation', 0.523)

        self.cmd_vel_timeout =  rospy.get_param('~cmd_vel_timeout', 0.3)

        self.max_vel = rospy.get_param('~max_vel', 1)
        self.min_vel = rospy.get_param('~min_vel', 1)

        self.left_vel = 0.0
        self.right_vel = 0.0

        self.last_cmd_vel_time = rospy.get_time()

    def init_ros(self):
        rospy.Subscriber('/cmd_vel', Twist, self.cmd_vel_cb)
        self.motor_status_pub = rospy.Publisher('motor_status', String, queue_size=10)

    """

      ####    ##   #      #      #####    ##    ####  #    #
     #    #  #  #  #      #      #    #  #  #  #    # #   #
     #      #    # #      #      #####  #    # #      ####
     #      ###### #      #      #    # ###### #      #  #
     #    # #    # #      #      #    # #    # #    # #   #
      ####  #    # ###### ###### #####  #    #  ####  #    #

    """
    def cmd_vel_cb(self, msg):
        self.last_cmd_vel_time = rospy.get_time()

        lin_vel = msg.linear.x
        ang_vel = msg.angular.z
        self.left_vel = lin_vel - (ang_vel * self.wheel_separation / 2)
        self.right_vel = lin_vel + (ang_vel * self.wheel_separation / 2)

    """

     ###### #    # #    #  ####  ##### #  ####  #    #
     #      #    # ##   # #    #   #   # #    # ##   #
     #####  #    # # #  # #        #   # #    # # #  #
     #      #    # #  # # #        #   # #    # #  # #
     #      #    # #   ## #    #   #   # #    # #   ##
     #       ####  #    #  ####    #   #  ####  #    #

    """    
        
    def write_driver_left(self, vel):
        # Send to Modbus
        if abs(vel) > self.min_vel:
            print(vel)

    def write_driver_right(self, vel):
        # Send to Modbus
        if abs(vel) > self.min_vel:
            print(vel)

    """

     #       ####   ####  #####
     #      #    # #    # #    #
     #      #    # #    # #    #
     #      #    # #    # #####
     #      #    # #    # #
     ######  ####   ####  #

    """
        
    def poll(self):
        if rospy.get_time() - self.last_cmd_vel_time > self.cmd_vel_timeout:
            self.left_vel = 0.0
            self.right_vel = 0.0
        self.write_driver_left(self.left_vel)
        self.write_driver_right(self.right_vel)
            
    
def main():
    diff_driver = DiffDriver()

if __name__ == '__main__':
    main()